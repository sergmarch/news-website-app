# News Website 📰

A simple React web application that shows the news in two sections:
  1. The main news overview, sorted by popularity
  2. The "Hot News" shows most recently added news

The app is responsive and mobile-friendly for seamless use across devices.

### [➡️ Click here to see it live ⬅️](https://66507fa130d7c52990a82dc1--scintillating-douhua-928d8d.netlify.app/)


## Technologies and features

- **React with TypeScript:** The latest version of React with TypeScript
- **Vite**: Build tool
- **ESLint**: Linting tool
- **Sass**: CSS preprocessor for styling
- **Jest**: Testing framework
- **Responsiveness**: Consistent user experience across devices
- **Keyboard navigation support**: Make sure the web app is accessible
- **Loading & Error states**

## Test plan

- The crucial application parts are covered with unit tests ("PopularNews" and "RecentNews")
- Other important aspects are covered with tests as well ("NewsListItem" and "Preview")
- Potential improvements: more unit tests, e2e tests

## Requirements

- Node.js (v21.7+)
- NPM (v10.5+)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Tests

```sh
npm run test
```
