export type NewsRecordKey = 'id' | 'title' | 'popularity' | 'timestamp'
export type NewsRecord = { [key in NewsRecordKey]: string }
