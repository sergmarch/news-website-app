import type { NewsRecord, NewsRecordKey } from "../types"

export function csvToJson(str: string): NewsRecord[] {
  const lines = str.split('\r\n')
  const headers = lines[0].split(';') as NewsRecordKey[]

  return lines
    .slice(1)
    .filter(Boolean)
    .map(line => {
      const values = line.split(';')
      const obj = {} as NewsRecord
      headers.forEach((header, i) => {
        obj[header] = values[i]
      })
      return obj
    })
}

export function sortByPopularity(news: NewsRecord[]) {
  return news.slice().sort((a, b) => parseFloat(b.popularity) - parseFloat(a.popularity))
}

export function sortByTimestamp(news: NewsRecord[]) {
  return news.slice().sort((a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime())
}

export function formatTimestamp(dateString: string) {
  const date = new Date(dateString)
  const hours = date.getUTCHours().toString().padStart(2, '0')
  const minutes = date.getUTCMinutes().toString().padStart(2, '0')

  return `${hours}:${minutes}`
}

