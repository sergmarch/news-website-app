import styles from './Header.module.sass'

function Header() {
  return (
    <header className={styles.header}>
      <h2 className={styles.headline}>
        The News Website | News from around the world
      </h2>
    </header>
  )
}

export default Header