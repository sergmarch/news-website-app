import type { NewsRecord } from '../../types'
import NewsListItem from '../NewsListItem/NewsListItem'
import styles from './NewsList.module.sass'

type NewsListProps = {
  news: NewsRecord[]
  timeline?: boolean
}

function NewsList({ news, timeline }: NewsListProps) {
  return (
    <ul className={styles.newsList}>
      {news.map(newsItem => <NewsListItem key={newsItem.id} newsItem={newsItem } timeline={timeline} />)}
    </ul>
  )
}

export default NewsList