import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Preview from './Preview'

describe('Preview', () => {
  test('renders correctly with given title', () => {
    const title = 'Test Title'

    render(<Preview title={title} />)

    const imgElement = screen.getByAltText('Preview image')
    const titleElement = screen.getByTestId('headline-title')

    expect(imgElement).toBeInTheDocument()
    expect(titleElement).toBeInTheDocument()
    expect(titleElement).toHaveTextContent(title)
  })
})