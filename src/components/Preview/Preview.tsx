import previewImg from '../../assets/ikea.jpeg'
import styles from './Preview.module.sass'

type PreviewProps = {
  title: string
}

function Preview({ title }: PreviewProps) {
  return (
    <a href="#">
      <figure className={styles.preview}>
        <img className={styles.img} src={previewImg} alt="Preview image"/>
        <figcaption className={styles.caption} data-testid="headline-title">{title}</figcaption>
      </figure>
    </a>
  )
}

export default Preview