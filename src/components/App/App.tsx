import { useNews } from '../../hooks'
import styles from './App.module.sass'
import RecentNews from '../RecentNews/RecentNews'
import PopularNews from '../PopularNews/PopularNews'
import Header from '../Header/Header'
import Spinner from '../Spinner/Spinner'
import ErrorMessage from '../ErrorMessage/ErrorMessage'

function App() {
  const { newsByPopularity, newsByTimestamp, loading, error } = useNews()

  if (loading) return <Spinner />
  if (error) return <ErrorMessage />

  return (
    <div className={styles.app}>
      <Header />
      <div className={styles.content}>
        <PopularNews news={newsByPopularity} />
        <RecentNews news={newsByTimestamp} />
      </div>
    </div>
  )
}

export default App
