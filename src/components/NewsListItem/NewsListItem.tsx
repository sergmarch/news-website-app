import { formatTimestamp } from '../../utils'
import styles from './NewsListItem.module.sass'
import type { NewsRecord } from '../../types'

type NewsListItemProps = {
  newsItem: NewsRecord
  timeline?: boolean
}

function NewsListItem({ newsItem, timeline }: NewsListItemProps) {
  // fake link url to be able to see the `visited` state, `r` - recent, `p` - popular
  const uniqLinkUrl = `#${newsItem.id}${timeline ? 'r' : 'p'}`

  return (
    <li className={`${styles.newsListItem} ${timeline && styles.newsListItemTimeline}`}>
      <a href={uniqLinkUrl} className={styles.newsListItemLink}>
        <div className={styles.newsListItemInfo}>
          {timeline && <time className={styles.newsListItemTime} data-testid="news-list-item-time">{formatTimestamp(newsItem.timestamp)}</time>}
          <p className={styles.newsListItemText} data-testid="news-list-item-text">{newsItem.title}</p>
        </div>
        {timeline && <span className={styles.newsListItemArrow} data-testid="news-list-item-arrow">›</span>}
      </a>
    </li>)
}

export default NewsListItem