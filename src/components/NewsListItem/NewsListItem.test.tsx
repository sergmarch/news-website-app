import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { formatTimestamp } from '../../utils'
import NewsListItem from './NewsListItem'

describe('NewsListItem Component', () => {
  const mockNewsItem = {
    id: '123456',
    title: 'Test News',
    timestamp: '2024-05-25T12:00:00.000Z',
    popularity: '0.5'
  }

  test('renders correctly with provided NewsRecord', () => {
    render(<NewsListItem newsItem={mockNewsItem} />)

    // Check if the link is rendered with correct href
    const linkElement = screen.getByRole('link')
    expect(linkElement).toHaveAttribute('href', `#${mockNewsItem.id}p`)

    const titleElement = screen.getByTestId('news-list-item-text')
    expect(titleElement).toBeInTheDocument()
    expect(titleElement).toHaveTextContent(mockNewsItem.title)
    expect(screen.queryByTestId('news-list-item-time')).not.toBeInTheDocument()
  })

  test('renders correctly with timeline', () => {
    render(<NewsListItem newsItem={mockNewsItem} timeline={true} />)

    // Check if the timestamp is rendered when timeline is true
    const timeElement = screen.getByTestId('news-list-item-time')
    expect(timeElement).toBeInTheDocument()
    expect(timeElement).toHaveTextContent(formatTimestamp(mockNewsItem.timestamp))

    // Check if the arrow is rendered when timeline is true
    const arrowElement = screen.getByTestId('news-list-item-arrow')
    expect(arrowElement).toBeInTheDocument()
    expect(arrowElement).toHaveTextContent('›')
  })
})
