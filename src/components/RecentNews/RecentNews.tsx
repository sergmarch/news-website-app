import NewsList from '../NewsList/NewsList'
import styles from './RecentNews.module.sass'
import type { NewsRecord } from '../../types'

type RecentNewsProps = {
  news: NewsRecord[]
}

function RecentNews({ news }: RecentNewsProps) {
  return (
    <aside className={styles.recentNews}>
      <h3>Latest news</h3>
      <NewsList news={news} timeline={true} />
    </aside>
  )
}

export default RecentNews