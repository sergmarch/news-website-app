import fs from 'fs'
import path from 'path'
import { render, screen } from '@testing-library/react'
import RecentNews from './RecentNews'
import { sortByTimestamp, csvToJson } from '../../utils'
import type { NewsRecord } from '../../types'

describe('RecentNews', () => {
  let newsByTimestamp: NewsRecord[] = []

  beforeAll(() => {
    const csvFilePath = path.resolve(__dirname, '../../../public/newsSource.csv')
    const csvText = fs.readFileSync(csvFilePath, 'utf-8')
    const newsData = csvToJson(csvText)
    newsByTimestamp = sortByTimestamp(newsData)
  })

  test('renders news sorted by timestamp', () => {
    render(<RecentNews news={newsByTimestamp} />)

    const newsTitles = screen.getAllByTestId('news-list-item-text').map(item => item.textContent)
    const sortedTitles = newsByTimestamp.map((item) => item.title)

    expect(newsTitles).toEqual(sortedTitles)
  })
})
