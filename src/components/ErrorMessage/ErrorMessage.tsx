import styles from './ErrorMessage.module.sass'

function ErrorMessage() {
  return <div className={styles.errorMessage}>Oops! Something went wrong. Please reload the page.</div>
}

export default ErrorMessage