import Preview from '../Preview/Preview'
import NewsList from '../NewsList/NewsList'
import styles from './PopularNews.module.sass'
import type { NewsRecord } from '../../types'

type PupularNewsProps = {
  news: NewsRecord[]
}

function PopularNews({ news }: PupularNewsProps) {
  const [headline, ...rest] = news

  return (
    <section className={styles.popularNews}>
      <Preview title={headline.title} />
      <NewsList news={rest} />
    </section>
  )
}

export default PopularNews