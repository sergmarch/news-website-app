import fs from 'fs'
import path from 'path'
import { render, screen } from '@testing-library/react'
import PopularNews from './PopularNews'
import { sortByPopularity, csvToJson } from '../../utils'
import type { NewsRecord } from '../../types'

describe('PopularNews', () => {
  let newsByPopularity: NewsRecord[] = []

  beforeAll(() => {
    const csvFilePath = path.resolve(__dirname, '../../../public/newsSource.csv')
    const csvText = fs.readFileSync(csvFilePath, 'utf-8')
    const newsData = csvToJson(csvText)
    newsByPopularity = sortByPopularity(newsData)
  })

  test('renders news sorted by popularity', () => {
    render(<PopularNews news={newsByPopularity} />)

    const newsTitles = screen.getAllByTestId('news-list-item-text').map(item => item.textContent)
    const previewImageTitle = screen.getByTestId('headline-title').textContent
    const allTitlesFromComponent = [previewImageTitle, ...newsTitles]
    const sortedTitles = newsByPopularity.map((item) => item.title)

    expect(allTitlesFromComponent).toEqual(sortedTitles)
  })
})
