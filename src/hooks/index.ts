import { useState, useEffect } from "react"
import { csvToJson, sortByPopularity, sortByTimestamp } from "../utils"
import type { NewsRecord } from "../types"

export function useNews() {
  const [news, setNews] = useState<NewsRecord[]>([])
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)

  useEffect(() => {
    const fetchNews = async () => {
      try {
        const response = await fetch('./newsSource.csv')
        if (response.status !== 200) {
          throw new Error('Failed to fetch news')
        }

        const csvText = await response.text()
        const newsRecords = csvToJson(csvText)

        setNews(newsRecords)
      } catch (err) {
        setError(err.message)
      } finally {
        setLoading(false)
      }
    }

    fetchNews()
  }, [])

  return {
    newsByPopularity: sortByPopularity(news),
    newsByTimestamp: sortByTimestamp(news),
    loading,
    error
  }
}