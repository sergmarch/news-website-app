import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './components/App/App.tsx'
import './assets/index.sass'

ReactDOM.createRoot(document.getElementById('root')!).render(<App />)
